/**
 * Displays tips and result code for the {@link Reversi} game (reversiGamePage.jsp)
 *
 * @author Igor Khlaponin igor.boxmails@gmail.com
 */

function displayTips() {
    $.ajax({
        method: 'GET',
        url: 'ajax/tips?gameId=' + $('#gameId').val(),
        success: function (tips) {
            if (tips.message == 'GAME OVER') {
                window.location.replace('finish?gameId=' + $('#gameId').val());
            }
            $('#tips').html(tips.message)
        }
    });
}


function refreshResultCode() {
    $.ajax({
        method: 'GET',
        url: 'reversi/getResultCode',
        data: 'gameId=' + $('#idValue').val(),
        success: function (data) {
            var resultCode;

            switch (data) {
                case 'OK':
                {
                    resultCode = data;
                    $('#resultCode').text(resultCode).css('background-color', 'greenyellow');
                    break;
                }
                case 'BAD_PLAYER':
                {
                    resultCode = data;
                    $('#resultCode').text(resultCode).css('background-color', 'red');
                    break;
                }
                case 'BAD_FIRST_PLAYER_ORDER':
                {
                    resultCode = data;
                    $('#resultCode').text(resultCode).css('background-color', 'red');
                    break;
                }
                case 'BAD_SECOND_PLAYER_ORDER':
                {
                    resultCode = data;
                    $('#resultCode').text(resultCode).css('background-color', 'red');
                    break;
                }
                case 'BAD_TURN_ORDER':
                {
                    resultCode = data;
                    $('#resultCode').text(resultCode).css('background-color', 'red');
                    break;
                }
                case 'BAD_TURN_LOGIC':
                {
                    resultCode = data;
                    $('#resultCode').text(resultCode).css('background-color', 'red');
                    break;
                }
                case 'BAD_TURN_FOR_FINISHED_GAME':
                {
                    resultCode = data;
                    $('#resultCode').text(resultCode).css('background-color', 'red');
                    break;
                }
                case 'BAD_TURN_FOR_NOT_STARTED_GAME':
                {
                    resultCode = data;
                    $('#resultCode').text(resultCode).css('background-color', 'red');
                    break;
                }
                default:
                {
                    resultCode = 'UNKNOWN STATUS';
                    $('#resultCode').text(resultCode).css('background-color', 'yellow');
                }
            }

        },
        error: function (errorThrown) {
            console.log("Error in getResultCode.js");
        }
    });
}
