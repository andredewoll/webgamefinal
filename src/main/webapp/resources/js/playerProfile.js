/**
 * Provides functions for playerProfile.jsp
 *
 * @author Igor Khlaponin igor.boxmails@gmail.com
 */

$(document).ready(function() {
    makeProfileActive();
    hidePopUp();
});

function makeProfileActive() {
    $('#profile').addClass("active");
    console.log('make profile active');
}


/**
 * methods for showing and hiding pop up window for avatar changing
 */
function showPopUp() {
    $("#avatar-p").show();
}

function hidePopUp() {
    $("#avatar-p").hide();
}

$(document).mouseup(function (e) {
    var container = $("#avatar-p");
    if (container.has(e.target).length === 0){
        container.hide();
    }
});