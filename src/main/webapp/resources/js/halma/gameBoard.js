$(document).ready(function () {
    displayTips();
    refreshBoard();
    setInterval(function () {
        displayTips();
        refreshBoard();
    }, 5000); //5 seconds
});


function refreshBoard() {
    console.log("in refreshBoard");
    $.ajax({
        method: 'GET',
        url: 'ajax/board?gameId=' + $('#gameId').val(),
        success: function (board) {
            drawBoard(board);
        }
    });
}

function displayTips() {
    $.ajax({
        method: 'GET',
        url: 'ajax/tips?gameId=' + $('#gameId').val(),
        success: function (tips) {
            if (tips.message == 'GAME OVER') {
                window.location.replace('finish?gameId=' + $('#gameId').val());
            }
            $('#tips').html(tips.message)
        }
    });
}

function drawBoard(board) {
    if (board == null) {
        return;
    }
    $('#board').html('');

    $.each(board, function (idx, cell) {
        if (idx != 0 && idx % 16 == 0) {
            $('#board').append("<br>");
        }

        if (cell.state == "EMPTY") {
            $('#board').append('<img id="' + cell.address + '" class="cell" src=resources/img/halma/emptycell.png>');
        }
        if (cell.state == "BUSY_BY_FIRST_PLAYER") {
            $('#board').append('<img id="' + cell.address + '" class="cell" src=resources/img/halma/greencell.png>')
        }
        if (cell.state == "BUSY_BY_SECOND_PLAYER") {
            $('#board').append('<img id="' + cell.address + '" class="cell" src=resources/img/halma/bluecell.png>')
        }
    });

    makeBoardDaggableAndDroppable();
}

function makeBoardDaggableAndDroppable() {
    $('.cell').draggable({
        containment: "parent",
        revert: 'invalid'
    });

    $('.cell').droppable({
        drop: function (ev, ui) {
            var dragged = ui.draggable;
            var droppedOn = $(this);

            var draggedColor = dragged.attr('src');
            var droppedOnColor = droppedOn.attr('src');
            var draggedId = dragged.attr('id');
            var droppedOnId = droppedOn.attr('id');

            $(droppedOn).droppable("disable");
            $(dragged).droppable("enable");
            $(dragged).attr('src', droppedOnColor);
            $(droppedOn).attr('src', draggedColor);


            var turn = draggedId + ',' + droppedOnId;
            var gameId = $('#gameId').attr('value');

            makeTurnViaAjax(gameId, turn);
        }
    });

    $('.cell').dblclick(function () {
        console.log('in dblClick');
        var gameId = $('#gameId').attr('value');
        var turn = $(this).attr('id') + ',' + $(this).attr('id');
        console.log(turn);
        makeTurnViaAjax(gameId, turn);
    });
}

function makeTurnViaAjax(gameId, turn) {
    $.ajax({
        type: "POST",
        url: 'ajax/maketurn',
        data: {gameId: gameId, turn: turn},
        success: function (data) {
            console.log("success");
            displayTips();
            refreshBoard();
        },
        error: function () {
            alert('Error!');
        }
    });
}

