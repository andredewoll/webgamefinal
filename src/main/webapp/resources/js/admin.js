$(document).ready(function () {
    deletePlayer();
    banPlayer();
    unbanPlayer();
    setAdminRole();
    setPlayerRole();
    fillModalEmailWithData();
    sendEmail();
});

function deletePlayer(){
    $('.deleteButton').click(function(){
        var playerId = $(this).parent().attr('id');
        $.ajax({
            method: 'POST',
            url: 'admin/delete/' + playerId,
            success: function (data) {
                console.log("success");
                location.reload();
            },
            error: function () {
                console.log("error");
            }
        })
    });
}

function banPlayer(){
    $('.banButton').click(function(){
        var playerId = $(this).parent().attr('id');
        banUnbanAjax(playerId, false);
    });
}

function unbanPlayer(){
    $('.unbanButton').click(function(){
        var playerId = $(this).parent().attr('id');
        banUnbanAjax(playerId, true);
    });
}

function banUnbanAjax(playerId, enabled) {
    $.ajax({
        method: 'POST',
        url: 'admin/enable/' + playerId,
        data: {enabled: enabled},
        success: function (data) {
            console.log("success");
            location.reload();
        },
        error: function () {
            console.log("error");
        }
    })
}

function setPlayerRole(){
    $('.adminButton').click(function(){
        var playerId = $(this).parent().attr('id');
        setAdminAjax(playerId, false);
    });
}

function setAdminRole(){
    $('.playerButton').click(function(){
        var playerId = $(this).parent().attr('id');
        setAdminAjax(playerId, true);
    });
}

function setAdminAjax(playerId, isAdmin) {
    $.ajax({
        method: 'POST',
        url: 'admin/setAdmin/' + playerId,
        data: {isAdmin: isAdmin},
        success: function (data) {
            console.log("success");
            location.reload();
        },
        error: function () {
            console.log("error");
        }
    })
}

function fillModalEmailWithData(){
    $('#emailModal').on('show.bs.modal', function (e) {
        var relatedA = $(e.relatedTarget).prev('a');
        var relatedLi = e.relatedTarget.closest('li');

        if (relatedA!=null && relatedLi!=null){
            $(this).find('.modal-title').html('Send email to: ' + relatedA.text());
            $(this).find('#playerId').val(relatedLi.id);
            $(this).find('#emailForm').attr('action', '/admin/sendEmail')
        }
        else {
            $(this).find('.modal-title').html('Send email to all');
            $(this).find('#emailForm').attr('action', '/admin/sendEmailToAll')
        }
    })
}

function sendEmail(){
    $('#sendEmailButton').click(function(){
        $('#emailForm').submit();
    });
}