package com.softserveinc.ita.multigame.controllers;

import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.confirmemail.OnRegistrationCompleteEvent;
import com.softserveinc.ita.multigame.model.confirmemail.VerificationToken;
import com.softserveinc.ita.multigame.services.PlayerService;
import com.softserveinc.ita.multigame.services.VerificationTokenService;
import com.softserveinc.ita.multigame.services.utils.PasswordEncryptor;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.time.LocalDateTime;

/**
 * Represents index.jsp, registration.jsp, badToken.jsp
 *
 * After success login redirect to "/list".
 *
 * Validate {@link Player} during registration. After success registration, saves it to DB
 * and send confirm email. After error return to registration.jsp.
 *
 * Check {@link VerificationToken} in DB. If success, make {@link Player} enabled for enter to game
 *
 * @author Mikhail Shvets shvetsmihail@gmail.com
 * @author Daniel Sokyrynskyi sokirinskiy@gmail.com
 * @author Igor Khlaponin igor.boxmails@gmail.com
 */

@Controller
@SessionAttributes("player")
public class LoginRegisterController {

    private Logger logger = Logger.getLogger(LoginRegisterController.class);

    @Autowired
    ApplicationEventPublisher eventPublisher;
    @Autowired
    VerificationTokenService verificationTokenService;
    @Autowired
    PlayerService playerService;
    @Autowired
    PasswordEncryptor passwordEncryptor;

    @RequestMapping("/")
    public String startPage() {
        return "index";
    }

    @RequestMapping("/registration")
    public String registerPage() {
        return "registration";
    }

    /**
     * Creates new instance of {@link com.softserveinc.ita.multigame.model.Player}
     * This instance will be set by Spring validator with data from the form on registration.jsp
     */
    @ModelAttribute("newPlayer")
    public Player createPlayerModel() {
        return new Player();
    }


    /**
     * Registration of new player
     *
     * @param player - is the player, which made by Spring for validation
     * @param bindingResult contains results of validation. If bindingResult has errors, controller
     *                      redirects to the registration.jsp again with pointing on this error.
     *                      If bindingResult has no errors, controller encrypts password form the form,
     *                      stores created player into the database and send email for registration confirmation
     */
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String registerNewPlayer(@ModelAttribute("newPlayer") @Valid Player player,
                                    BindingResult bindingResult,
                                    HttpServletRequest request,
                                    ModelMap modelMap) {

        if (bindingResult.hasErrors()) {
            logger.info("Errors during field fulfilling. Returning to registration form");
            return "registration";
        }

        if (playerService.getByLogin(player.getLogin()) == null) {
            //get row password, which set during creation
            String password = player.getPassword();
            //set encrypted password for storing in database
            player.setPassword(passwordEncryptor.encode(password));
            player.setRegistrationTime(LocalDateTime.now());
            String appPath = request.getRequestURL().substring(0, request.getRequestURL().indexOf("/register"));
            logger.debug("appPath: " + appPath);
            player.setAvatar(appPath + "/resources/avatars/default.png");
            //storing new player into database
            playerService.saveOrUpdate(player);

            eventPublisher.publishEvent(new OnRegistrationCompleteEvent(player, appPath));

            modelMap.addAttribute("message", "please, confirm your email");

            logger.info("Success registration form fulfilling. Sent email and returning to login page");
        }
        return "index";
    }

    @RequestMapping(value = "/registrationConfirm", method = RequestMethod.GET)
    public String confirmRegistration (@RequestParam("token") String token){
        VerificationToken verificationToken = verificationTokenService.getVerificationToken(token);

        logger.debug("verificationToken was gotten from db");

        if (verificationToken == null) {
            logger.debug("bad Token");
            return "badToken";
        }

        Player player = verificationToken.getPlayer();
        player.setEnabled(true);
        playerService.saveOrUpdate(player);
        logger.debug("player set to enabled");
        return "redirect:/";
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String register() {
        return "redirect:/registration";
    }

    @RequestMapping(value = "/login")
    public String loginSuccess(ModelMap map, HttpServletRequest request) {
        String login = request.getRemoteUser();
        Player player = playerService.getByLogin(login);
        map.addAttribute("player", player);
        return "redirect:/list";
    }
}
