package com.softserveinc.ita.multigame.controllers;

import com.softserveinc.ita.multigame.model.Game;
import com.softserveinc.ita.multigame.model.GameType;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.confirmemail.VerificationToken;
import com.softserveinc.ita.multigame.model.engine.GameDTO;
import com.softserveinc.ita.multigame.services.GameHistoryService;
import com.softserveinc.ita.multigame.services.GameListService;
import com.softserveinc.ita.multigame.services.PlayerService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Create {@link Game} for current {@link Player} with special {@link GameType}
 * Join to {@link Game} - set current {@link Player} as second to received {@link Game}
 * Return wantJoin.jsp when current {@link Player} want to join to "waiting game";
 *
 * @author Mikhail Shvets shvetsmihail@gmail.com
 * @author Daniel Sokyrynskyi sokirinskiy@gmail.com
 * @author Vladimir Serdiuk vvserdiuk@gmail.com
 */

@Controller
public class GameListController {
    private Logger logger = Logger.getLogger(GameListController.class);

    @Autowired
    GameListService gameListService;
    @Autowired
    PlayerService playerService;

    @Autowired
    GameHistoryService gameHistoryService;

    @RequestMapping("/list")
    public String mainPage(@SessionAttribute Player player, ModelMap map) {
        logger.debug("MainPage");
        return "gameList";
    }

    @ResponseBody
    @RequestMapping("/ajax/lists")
    public Map<String, Object> refreshLists(@SessionAttribute Player player) {
        logger.debug("refreshLists");
        List<GameDTO> createdGames = gameListService.getCreatedGamesDTOs(player);
        List<GameDTO> playingGames = gameListService.getPlayingGamesDTOs(player);
        List<GameDTO> waitingGames = gameListService.getWaitingGamesDTOs(player);

        Map<String, Object> data = new HashMap<>();
        data.put("playingGames", playingGames);
        data.put("createdGames", createdGames);
        data.put("waitingGames", waitingGames);

        return data;
    }

    @RequestMapping("/create")
    public String createGame(@SessionAttribute Player player,
                             @RequestParam("gameType") String gameType) {
        gameListService.createGame(player, GameType.valueOf(gameType));
        logger.info("CreateGame gameType is: " + gameType);
        return "redirect:/list";
    }

    @RequestMapping("/join")
    public String joinGame(@SessionAttribute Player player,
                           // @RequestParam("gameId") Long gameId1,
                           @ModelAttribute("gameId") Long gameId) {
        Game game = gameListService.getGame(gameId);

        /**
         *Here we add secondPlayer into the game,
         * or NOT if it's SeaBattle
         */
        if (game.getGameType() == GameType.SEABATTLE) {
            return "seaBattle/setShips";
        } else {
            game.joinToGame(player);
            logger.info("Join gameId is: " + gameId);
            return "redirect:/list";
        }
    }

    @RequestMapping("/wantjoin")
    public String wantToJoinGame(@RequestParam("gameId") Long gameId,
                                 @RequestParam("gameOwnerId") Long gameOwnerId,
                                 Model model) {
        Player gameOwner = playerService.get(gameOwnerId);
        GameType game = gameListService.getGame(gameId).getGameType();
        model.addAttribute("gameOwner", gameOwner);
        model.addAttribute("gameId", gameId);
        model.addAttribute("gameName", game);
        logger.info("WantToJoinGame gameId is: " + gameId + " gameOwnerId is: " + gameOwnerId);
        return "wantJoin";
    }
    @RequestMapping("/goChat")
    public String wantToJoinChat() {
        return "chat";
    }
}
