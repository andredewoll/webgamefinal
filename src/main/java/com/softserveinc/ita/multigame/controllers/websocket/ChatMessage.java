package com.softserveinc.ita.multigame.controllers.websocket;

import lombok.Data;
/**
 * Message Object
 *
 * @author Daniel Sokyrynskyi sokirinskiy@gmail.com
 */
@Data
public class ChatMessage {
    private String player;
    private String text;
    private String avatar;
}
