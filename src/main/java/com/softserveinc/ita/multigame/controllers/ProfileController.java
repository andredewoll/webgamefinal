package com.softserveinc.ita.multigame.controllers;

import com.softserveinc.ita.multigame.model.GameHistory;
import com.softserveinc.ita.multigame.model.Gender;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.services.GameHistoryService;
import com.softserveinc.ita.multigame.services.PlayerService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.List;

/**
 * Controller for playerProfile.jsp page
 *
 * Contains methods for changing profile for current player
 *
 * @author Igor Khlaponin igor.boxmails@gmail.com
 *
 */

@Controller
public class ProfileController {

    private Logger logger = Logger.getLogger(ProfileController.class);

    private PlayerService playerService;
    private GameHistoryService gameHistoryService;
    @Autowired
    public ProfileController(PlayerService playerService, GameHistoryService gameHistoryService) {
        this.gameHistoryService = gameHistoryService;
        this.playerService = playerService;
    }

    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    public String profilePage(@RequestParam("id") Long playerId, ModelMap map) {

        Player player = playerService.get(playerId);

        List<GameHistory> gameHistoryList = gameHistoryService.getGameHistoriesForPlayer(player);

        map.put("displayedPlayer", player);
        map.put("gameHistoryList", gameHistoryList);
        logger.info("redirect to the player profile page");

        return "playerProfile";
    }

    @RequestMapping(value = "/updateProfile", method = RequestMethod.POST)
    public String updateProfile(HttpServletRequest request, ModelMap map) {
        Player player = playerService.getByLogin(request.getParameter("login"));

        if (!request.getParameter("fullName").equals("")) {
            player.setFullName(request.getParameter("fullName"));
        }

        if (!request.getParameter("email").equals("")) {
            player.setEmail(request.getParameter("email"));
        }

        if (!request.getParameter("gender").equals("null")) {
            player.setGender(Gender.valueOf(request.getParameter("gender")));
        }
        if (!request.getParameter("birthday").equals("") || request.getParameter("birthday").equals("\'\'")) {
            player.setBirthdayDate(LocalDate.parse(request.getParameter("birthday")));
        }

        playerService.saveOrUpdate(player);
        logger.info("updated player " + player.getLogin());

        List<GameHistory> gameHistoryList = gameHistoryService.getGameHistoriesForPlayer(player);
        map.put("displayedPlayer", player);
        map.put("gameHistoryList", gameHistoryList);

        return "redirect:/profile?id=" + player.getId();
    }

    @RequestMapping("/players")
    public String getAllProfiles(ModelMap map) {
        List<Player> players = playerService.getAll();
        map.put("players", players);

        return "playersList";
    }

    @RequestMapping(value = "/changeAvatar", method = RequestMethod.POST)
    public String updateAvatar(@SessionAttribute Player player,
                               @RequestParam("avatar") MultipartFile uploadfile,
                               HttpServletRequest request) {
        String avatarPath;
        try {
            String fileName = uploadfile.getOriginalFilename();
            String directory = request.getServletContext().getRealPath("/resources/avatars");

            //real path to image storage
            avatarPath = Paths.get(directory, fileName).toString();

            BufferedOutputStream stream =
                    new BufferedOutputStream(new FileOutputStream(new File(avatarPath)));
            stream.write(uploadfile.getBytes());
            stream.close();

            //path with will be stored in DB (related to web app)
            String relatedPath = request.getRequestURL()
                    .substring(0, request.getRequestURL().indexOf("/changeAvatar"))
                    + "/resources/avatars/" + fileName;
            logger.info("====> new avatar stored on the disk");

            player.setAvatar(relatedPath);
            playerService.saveOrUpdate(player);
            logger.info("====> uploaded new avatar for " + player.getLogin() + ", which is stored in: " +
                    avatarPath);

        } catch (Exception e) {
            logger.info("avatar for " + player.getLogin() + " is not changed");
        }

        return "redirect:/profile?id=" + player.getId();
    }
}
