package com.softserveinc.ita.multigame.controllers.renju;

import com.softserveinc.ita.multigame.model.Game;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.engine.GameEngine;
import com.softserveinc.ita.multigame.services.GameListService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;


@Controller
@RequestMapping("/renju")
public class RenjuPageController {
    private Logger logger = Logger.getLogger(RenjuPageController.class);

	private GameListService gameListService;
	@Autowired
	public RenjuPageController(GameListService gameListService) {
		this.gameListService = gameListService;
	}

	@RequestMapping(method = RequestMethod.GET)
	public String getPage(@SessionAttribute Player player, @RequestParam("gameId") Long gameId, ModelMap model) {

        Game game = gameListService.getGame(gameId);
        GameEngine gameEngine = game.getGameEngine();

        Player firstPlayer = gameEngine.getFirstPlayer();
        Player secondPlayer = gameEngine.getSecondPlayer();

        Player opponent = (firstPlayer.equals(player)) ? secondPlayer : firstPlayer;

        model.addAttribute("gameId", gameId);
        model.addAttribute("currentPlayer", player);
        model.addAttribute("opponent", opponent);

        return "renju/renjuGame";
    }

}
