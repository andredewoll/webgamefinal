package com.softserveinc.ita.multigame.services;

import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.confirmemail.VerificationToken;
import com.softserveinc.ita.multigame.repositories.VerificationTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Implementation of {@link VerificationTokenService}
 *
 * @author Mikhail Shvets shvetsmihail@gmail.com
 */
@Service
@Transactional
public class VerificationTokenServiceImpl implements VerificationTokenService {

    @Autowired
    private VerificationTokenRepository repository;

    @Override
    public void createVerificationToken(Player player, String token) {
        VerificationToken verificationToken = new VerificationToken(token, player);
        repository.save(verificationToken);
    }

    @Override
    @Transactional(readOnly = true)
    public VerificationToken getVerificationToken(String token) {
        if (token == null) return null;
        return repository.findByToken(token);
    }
}
