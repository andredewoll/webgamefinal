package com.softserveinc.ita.multigame.services;

import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.confirmemail.VerificationToken;

/**
 *  Interface for working with {@link VerificationToken}
 *
 * @author Mikhail Shvets shvetsmihail@gmail.com
 */
public interface VerificationTokenService {

    void createVerificationToken(Player player, String token);

    VerificationToken getVerificationToken(String token);
}
