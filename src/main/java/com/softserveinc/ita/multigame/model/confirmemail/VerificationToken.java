package com.softserveinc.ita.multigame.model.confirmemail;

import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.engine.GameEngine;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Entity that is saved in DB and represent unique token for validating email.
 * Contains link to verifiable {@link Player}
 *
 * @author Mikhail Shvets shvetsmihail@gmail.com
 */
@Entity
public class VerificationToken {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String token;

    @OneToOne(targetEntity = Player.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "player_id")
    private Player player;

    private LocalDateTime regTime;

    public VerificationToken() {
    }

    public VerificationToken(String token, Player player) {
        this.token = token;
        this.player = player;
        regTime = LocalDateTime.now();
        player.setEnabled(false);
    }

    public String getToken() {
        return token;
    }

    public Player getPlayer() {
        return player;
    }

    public LocalDateTime getRegTime() {
        return regTime;
    }
}
