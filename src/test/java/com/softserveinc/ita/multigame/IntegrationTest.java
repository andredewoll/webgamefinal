package com.softserveinc.ita.multigame;

/**
 * Marker interface for running integration tests with maven-failsafe-plugin
 *
 * @author Artem Dvornichenko artem.dvornichenko@gmail.com advorntc
 */
public interface IntegrationTest {
}
