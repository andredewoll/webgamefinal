package com.softserveinc.ita.multigame.controllers.mill;


import com.softserveinc.ita.multigame.model.Game;
import com.softserveinc.ita.multigame.model.GameType;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.engine.GameEngine;
import com.softserveinc.ita.multigame.services.GameListService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.ui.ModelMap;

import static org.junit.Assert.assertEquals;

/**
 * Unit Testing Mill game controller
 *
 * @author Andrew Volyk andredewoll@gmail.com avolyk1tc
 */
public class MillPageMockTestController {

    Player player = new Player(1L, "Juice", "111");
    Player opponent = new Player(2L, "Vodka", "111");
    Long gameId = 2L;
    Game game = new Game(player, GameType.MILL);


    private GameListService gameListService;
    private MillPageController controller;

    @Before
    public void init() {
        gameListService = Mockito.mock(GameListService.class);
        controller = new MillPageController(gameListService);
    }
    @Test
    public void testingThatGetPageMillRedirectsToTheViewAndPutsAttributesTrue() {
        //Setup
        Mockito.when(gameListService.getGame(gameId)).thenReturn(game);
        GameEngine gameEngine = game.getGameEngine();
        player = gameEngine.getFirstPlayer();
        opponent = gameEngine.getSecondPlayer();
        ModelMap model = new ModelMap();
        //Execute
        String viewName = controller.getPage(player, gameId, model);
        //Verify
        assertEquals(viewName, "mill/millGame");
        assertEquals(model.get("player"), player);
        assertEquals(model.get("gameId"),gameId);
    }
}
