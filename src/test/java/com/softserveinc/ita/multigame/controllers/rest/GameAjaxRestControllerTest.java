package com.softserveinc.ita.multigame.controllers.rest;

import com.softserveinc.ita.multigame.IntegrationTest;
import com.softserveinc.ita.multigame.model.Game;
import com.softserveinc.ita.multigame.model.GameListManager;
import com.softserveinc.ita.multigame.model.GameType;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.services.GameListService;
import com.softserveinc.ita.multigame.services.PlayerService;
import com.softserveinc.ita.multigame.services.TipDTO;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import com.softserveinc.ita.multigame.IntegrationTest;
import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Tests for {@link GameAjaxRestController}
 *
 * @author Andrew Volyk andredewoll@gmail.com avolyk1tc
 */

@Category(IntegrationTest.class)
@RunWith(SpringJUnit4ClassRunner.class)
@TestPropertySource(properties = "spring.datasource.url=jdbc:postgresql://localhost:5432/testmg")
@DirtiesContext
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
public class GameAjaxRestControllerTest {

    private final String BASE_URL = "/api/v.0.1";
    private GameType gameType = GameType.RENJU;

    @Autowired
    GameAjaxRestController controllerToTest;
    @Autowired
    PlayerService playerService;
    @Autowired
    GameListService gameListService;

    private MockMvc mockMvc;
    private Player p1;
    private Player p2;

    @Before
    public void setUp() {
        Player player1 = new Player("AAA", "aaa");
        player1.setEmail("aaa@gmail.com");
        playerService.saveOrUpdate(player1);
        Player player2 = new Player("BBB", "aaa");
        player2.setEmail("bbb@gmail.com");
        playerService.saveOrUpdate(player2);

        p1 = playerService.getByLogin("AAA");
        p2 = playerService.getByLogin("BBB");

        mockMvc = MockMvcBuilders.standaloneSetup(controllerToTest).build();
        controllerToTest = mock(GameAjaxRestController.class);
    }

    @Test
    public void testGetGameBoardById() throws Exception {

        Game newGame = gameListService.createAndReturnGame(p1, gameType);
        newGame.getGameEngine().getBoard();

        mockMvc.perform(get(BASE_URL + "/board?gameId=" + newGame.getGameEngine().getId())
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")));
    }

    @Test
    public void testGetTipsByGameId() throws Exception {

        Game newGame2 = gameListService.createAndReturnGame(p1, gameType);
        newGame2.joinToGame(p2);
        newGame2.makeTurn(p1, "1,1");
        newGame2.makeTurn(p2, "11,7");
        gameListService.getTips(1L, p2);

        mockMvc.perform(get(BASE_URL + "/board?gameId=" + newGame2.getGameEngine().getId())
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")));
    }

    @Test
    public void testMakeTurnByGameIdAndTun() throws Exception {

        Game newGame3 = gameListService.createAndReturnGame(p1, gameType);
        newGame3.joinToGame(p2);
        gameListService.makeTurn(newGame3.getGameEngine().getId(), p1, "11,8");

        mockMvc.perform(post((BASE_URL + "/maketurn?gameId=" +newGame3.getGameEngine().getId() +"&turn=11,8"))
                .sessionAttr("player", p2).contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

    }

}
